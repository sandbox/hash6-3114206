<?php
/**
 * @file
 */

namespace Drupal\propay_api\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\propay_api\ProtectPayApi;

/**
 * Contribute form.
 */
class PropayAccountNumberForm extends FormBase {


    const RESOURCE_URI = 'propayapi/signup';

    private $apiBaseUrl; // = 'https://xmltestapi.propay.com/';
    protected $endpoint;
    protected $billerId;
    protected $authToken;
    protected $certStr;
    protected $termId;
    /**
     * PropayAccountCreateForm constructor.
     */
    public function __construct() {
        $elements = \Drupal::state();
        $this->apiBaseUrl = $elements->get('propay_api_base_url'); //'https://xmltestapi.propay.com/';
        $this->resourceUri = self::RESOURCE_URI;
        $this->endpoint = $this->getEndpoint();
        $this->billerId = $elements->get('propay_api_biller_id');
        $this->authToken = $elements->get('propay_api_auth_token');
        $this->certStr = $elements->get('propay_api_cert_str');
        $this->termId =  $elements->get('propay_api_term_id');
    }




    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'create_propay_account_number_form';

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {


        $form['first_name'] = [
            '#type' => 'textfield',
            '#title' =>  $this->t('First Name'),
            '#required' => TRUE,
            '#maxlength' => 20,

        ];

        $form['last_name'] = [
            '#type' => 'textfield',
            '#title' =>  $this->t('Last Name'),
            '#required' => TRUE,
            '#maxlength' => 25,

        ];

        $form['day_phone'] = [
            '#type' => 'tel',
            '#title' =>  $this->t('Day Phone'),
            '#required' => TRUE,
            '#maxlength' => 10,
        ];

        $form['even_phone'] = [
            '#type' => 'tel',
            '#title' =>  $this->t('Evening Phone'),
            '#required' => TRUE,
            '#maxlength' => 10,
        ];

        $form['dob'] = [
            '#type' => 'date',
            '#title' =>  $this->t('Date of Birth'),
            '#required' => TRUE,
        ];

        $form['email'] = [
            '#type' => 'email',
            '#title' =>  $this->t('Email'),
            '#required' => TRUE,
        ];

        $form['ssn'] = [
            '#type' => 'textfield',
            '#title' =>  $this->t('Social Security Number'),
            '#required' => TRUE,
            '#maxlength' => 9,
        ];

        $form['address1'] = [
            '#type' => 'textfield',
            '#title' =>  $this->t('Address 1'),
            '#required' => TRUE,
            '#maxlength' => 100,

        ];

        $form['city'] = [
          '#type' => 'textfield',
          '#title' =>  $this->t('City'),
          '#required' => TRUE,
          '#maxlength' => 30,

        ];
        //@todo work on fetching all states
        $states = array_flip([
          "Alabama" => "AL",
          "Alaska" => "AK",
          "Arizona" => "AZ",
          "Arkansas" => "AR",
          "California" => "CA",
          "Colorado" => "CO",
          "Connecticut" => "CT",
          "Delaware" => "DE",
          "District of Columbia" => "DC",
          "Florida" => "FL",
          "Georgia" => "GA",
          "Hawaii" => "HI",
          "Idaho" => "ID",
          "Illinois" => "IL",
          "Indiana" => "IN",
          "Iowa" => "IA",
          "Kansas" => "KS",
          "Kentucky" => "KY",
          "Louisiana" => "LA",
          "Maine" => "ME",
          "Montana" => "MT",
          "Nebraska" => "NE",
          "Nevada" => "NV",
          "New Hampshire" => "NH",
          "New Jersey" => "NJ",
          "New Mexico" => "NM",
          "New York" => "NY",
          "North Carolina" => "NC",
          "North Dakota" => "ND",
          "Ohio" => "OH",
          "Oklahoma" => "OK",
          "Oregon" => "OR",
          "Maryland" => "MD",
          "Massachusetts" => "MA",
          "Michigan" => "MI",
          "Minnesota" => "MN",
          "Mississippi" => "MS",
          "Missouri" => "MO",
          "Pennsylvania" => "PA",
          "Rhode Island" => "RI",
          "South Carolina" => "SC",
          "South Dakota" => "SD",
          "Tennessee" => "TN",
          "Texas" => "TX",
          "Utah" => "UT",
          "Vermont" => "VT",
          "Virginia" => "VA",
          "Washington"  => "WA",
          "West Virginia"  => "WV",
          "Wisconsin" => "WI",
          "Wyoming" => "WY",
        ]);

        $form['state'] = [
          '#type' => 'select',
          '#title' =>  $this->t('State'),
          '#required' => TRUE,
          '#options' => $states,
        ];

        //@todo to implement for other countries,currently only for US

        $form['zip'] = [
            '#type' => 'textfield',
            '#title' =>  $this->t('Zip'),
            '#required' => TRUE,
        ];


        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Create'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $values = $form_state->getValues();

        $data = [
            'PersonalData' => [
                'FirstName' => $values['first_name'],
                'LastName' => $values['last_name'],
                'DateOfBirth' => $values['dob'],
                'SourceEmail' => $values['email'],
                'SocialSecurityNumber' => $values['ssn'],
                'PhoneInformation' => [
                    'DayPhone' => $values['day_phone'],
                    'EveningPhone' => $values['even_phone']
                ],
            ],
            'Address' => [
                'Address1' => $values['address1'],
                'City' => $values['city'],
                'State' => $values['state'],
                'Zip' => $values['zip'],
            ],
        ];

        $result = json_encode($data);
        $ch = curl_init($this->getEndpoint());
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $result );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->_getAuth());
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($result),
                $this->addBaseEncode(),
            ]
        );
        $response = json_decode(curl_exec($ch));

        $messenger = \Drupal::messenger();
        $profile_id = '';
        if ($response->Status == '00') {
            $protectPayAPI = new ProtectPayApi();
            $result = $protectPayAPI->setBillerId($this->billerId)
                ->setAuthToken($this->authToken)
                ->setMerchantProfileData($data)
                ->createMerchantProfile()
                ->getCreatedMerchantProfileInfo();
            $merchant_profile_response = json_decode($result);
            if ((isset($merchant_profile_response->RequestResult->ResultCode) && $merchant_profile_response->RequestResult->ResultCode == "00")
            ) {
                $profile_id = $merchant_profile_response->ProfileId;
                $messenger->addMessage(
                    t('Merchant Profile added is - '. $profile_id)
                );
            }

            $fields = [];
            $existing_fields = ['first_name','last_name','day_phone','even_phone','email','ssn','address1','city','state','zip'];
            foreach($values as $key => $value) {
                if (in_array($key, $existing_fields)) {
                  $messenger->addMessage(t($key . '-' . $value));
                  $fields['field_pa_' . $key] = $value;
                }
            }
            $fields['field_pa_profile_id'] = ($profile_id) ?? '';
            $fields['field_pa_account_number'] = $response->AccountNumber;
            $fields['field_pa_tier'] = $response->Tier;

            $fields['type'] = 'propay_api';
            $fields['field_pa_password'] = $response->Password;
            $fields['title'] = $values['first_name'] . ' - '. $values['last_name'] . '- '. $response->AccountNumber;
            //@todo : add an if condition to check if propay_api content type exists so that it does not report error
            $entity = \Drupal::entityTypeManager()->getStorage('node')->create($fields);
            //@todo to fix date of birth value not being captured
            $entity->set('field_pa_dob', $values['field_pa_dob']);
            $entity->save();
            $form_state->setRedirect('system.admin_content');
        }
        else {
          $messenger->addMessage(
              t('Error Occured with response code from Propay API:<em>'. $response->Status.'</em>'), 'error');
        }
    }

    public function addBaseEncode() {
      $encoded_value = base64_encode($this->certStr.':'.$this->termId);
      return 'Authorization: Basic '. $encoded_value;
    }

    public function _getAuth() {
      return $this->billerId .':'. $this->authToken;
    }

    public function getEndpoint() {
      return $this->endpoint = $this->apiBaseUrl . SELF::RESOURCE_URI;
    }
}
?>
