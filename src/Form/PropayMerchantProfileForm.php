<?php
/**
 * @file
 */

namespace Drupal\propay_api\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\propay_api\ProtectPayApi;

/**
 * Contribute form.
 */
class PropayMerchantProfileForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'create_merchant_profile_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
      $form['account-number'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' =>  $this->t('Account Number'),
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Create'),
      ];

      return $form;

    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $account_number = $form_state->getValue('account-number');
      //call protectpay api and generate merchant profile id and store it
        $elements = \Drupal::state();
        $data['ProfileName'] = 'ProPay';
        $data['PaymentProcessor'] = 'LegacyProPay';
        $data['ProcessorData'][] = [
            'ProcessorField' => 'certStr',
            'Value' => $elements->get('propay_api_cert_str'),
        ];

        $data['ProcessorData'][] = [
            'ProcessorField' => 'accountNum',
            'Value' => $account_number,//$value['account-number']
        ];

        $data['ProcessorData'][] = [
            'ProcessorField' => 'termId',
            'Value' => $elements->get('propay_api_term_id'),
        ];
        $data_string = json_encode($data);


        $protectPayAPI = new ProtectPayApi();
        $elements = \Drupal::state();
        $result = $protectPayAPI->setBillerId($elements->get('propay_api_biller_id'))
            ->setAuthToken($elements->get('propay_api_auth_token'))
            ->setMerchantProfileData($data)
            ->createMerchantProfile()
            ->getCreatedMerchantProfileInfo();
        $response = json_decode($result);

        if ((isset($response->RequestResult->ResultCode) && $response->RequestResult->ResultCode == "00")
        ) {
            $profile_id = $response->ProfileId;
            if ($account_number) {
                $propay_api = \Drupal::entityTypeManager()
                    ->getStorage('node')
                    ->loadByProperties(
                        [
                            'field_pa_account_number' => $account_number,
                            'type' => 'propay_api'
                        ]
                    );
                // save the profile id to node of type propay_api and field field_profile_id
                if ($propay_api) {
                    foreach($propay_api as $node) {
                        $node->field_pa_profile_id->value = $profile_id;
//                        $node->field_pa_tier->value = $response->Tier;
                        $node->save();
                        \Drupal::messenger()->addMessage(t('Propay Merchant Profile Id - '. $profile_id . ' saved successfully.'));
                    }
                }
                else {
                    \Drupal::messenger()->addMessage(t('No Propay Account with account number - '. $account_number . ' exists. Please try with different account number.'));
                }
            //save profile Id in the entity
                $form_state->setRedirect('system.admin_content');

            }
        }

        //Save the account number and profile id on node of type propay_api
      //and field field_propay_account_number
    }
}
?>
