<?php
/**
 * @file
 */

namespace Drupal\propay_api\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contribute form.
 */
class PropayAccountNumberEditForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'create_propay_account_number_edit_form';

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
    }
}
?>
