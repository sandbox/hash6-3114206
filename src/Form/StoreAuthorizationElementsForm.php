<?php
namespace Drupal\propay_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class StoreAuthorizationElementsForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'propay_api_store_auth_elements_form';
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
      $cert_str = \Drupal::state()->get('propay_api_cert_str');
      $term_id = \Drupal::state()->get('propay_api_term_id');
      $biller_id = \Drupal::state()->get('propay_api_biller_id');
      $auth_token = \Drupal::state()->get('propay_api_auth_token');
      $base_url   = \Drupal::state()->get('propay_api_base_url');

      $form['cert-str'] = [
        '#type' => 'textfield',
        '#title' => $this->t('CertStr'),
        '#required' => TRUE,
        '#default_value' => $cert_str,
      ];

      $form['term-id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('termID'),
        '#required' => TRUE,
        '#default_value' => $term_id,
      ];

      //billerid
        $form['biller-id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Biller ID'),
            '#required' => TRUE,
            '#default_value' => $biller_id,
        ];

      //authentication token
        $form['auth-token'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Authentication Token'),
            '#required' => TRUE,
            '#default_value' => $auth_token,
        ];

        $form['base-url'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Base Url'),
            '#required' => TRUE,
            '#default_value' => $base_url,
        ];

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Submit'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      // Retrieve the configuration.
      \Drupal::state()->set('propay_api_cert_str', $form_state->getValue('cert-str'));
      \Drupal::state()->set('propay_api_term_id',$form_state->getValue('term-id'));
      \Drupal::state()->set('propay_api_biller_id',$form_state->getValue('biller-id'));
      \Drupal::state()->set('propay_api_auth_token',$form_state->getValue('auth-token'));
      \Drupal::state()->set('propay_api_base_url',$form_state->getValue('base-url'));

    }

}
