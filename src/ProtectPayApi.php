<?php

namespace Drupal\propay_api;
/*
ProPay provides the following code "AS IS."
ProPay makes no warranties and ProPay disclaims all warranties and conditions, express, implied or statutory,
including without limitation the implied warranties of title, non-infringement, merchantability, and fitness for a particular purpose.
ProPay does not warrant that the code will be uninterrupted or error free,
nor does ProPay make any warranty as to the performance or any results that may be obtained by use of the code.
*/
class ProtectPayApi
{

/* change this to the production url for going live after testing https://api.propay.com */
private $_apiBaseUrl = 'https://xmltestapi.propay.com';

/* credentials that would normally be set from database values or a config value */
private $_billerId;
private $_authToken;

/* for creating merchant profiles */
private $_createMerchantProfileData;
private $_createdMerchantProfileInfo;

/**
* @param string $billerId
* @return $this
*/
public function setBillerId($billerId) {
$this->_billerId = $billerId;
return $this;
}

/**
* @param string $authToken
* @return $this
*/
public function setAuthToken($authToken) {
$this->_authToken = $authToken;
return $this;
}

/**
* @return string
*/
private function _getAuth() {
return $this->_billerId . ':' . $this->_authToken;
}

/**
* @return mixed
*/
public function getCreatedMerchantProfileInfo() {
return $this->_createdMerchantProfileInfo;
}

/**
* @param array $merchantProfileData
* @return $this
*/
public function setMerchantProfileData($merchantProfileData) {
$this->_createMerchantProfileData = $merchantProfileData;
return $this;
}

/**
 * Create the merchant profile
 * @return $this
 * _createdMerchantProfileInfo contains a json string like this
 * {
 * "RequestResult":
 * {
 * "ResultValue":"SUCCESS",
 * "ResultCode":"00",
 * "ResultMessage":""
 * },
 * "ProfileId":769987
 * }
*/

public function createMerchantProfile() {
  $data_string = json_encode($this->_createMerchantProfileData);
  $ch = curl_init($this->_apiBaseUrl . '/protectpay/merchantprofiles');
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_USERPWD, $this->_getAuth());
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string)
   )
  );
  $this->_createdMerchantProfileInfo = curl_exec($ch);
  return $this;
}

}
/**
$data = [
"ProfileName" => "ProPay",
"PaymentProcessor" => "LegacyProPay",
"ProcessorData" => [
{
"ProcessorField" => "certStr",
"Value" => "90a933bc-07e1-4d33-9a97-084929113a22"
},
{
"ProcessorField" => "accountNum",
"Value" => "30312345"
},
{
"ProcessorField" => "termId",
"Value": "113a22"
}
 ]
];

// Create Merchant Profile
$protectPayAPI = new ProtectPayApi();
$result = $protectPayAPI->setBillerId('9999986379225246')
->setAuthToken('16dfe8d7-889b-4380-925f-9c2c6ea4d930')
->setMerchantProfileData($data)
->createMerchantProfile()
->getCreatedMerchantProfileInfo();
**/
